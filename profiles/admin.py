from django.contrib.auth.models import User
from django.contrib import admin
from profiles.models import Profile

# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'bio', 'location', 'program', 'email', 'links', 'social_profiles']

    