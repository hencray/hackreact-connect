from django.db import models
from django.contrib.auth.models import User


# Create your models here.
PROGRAM_CHOICES = [
    ('hack_reactor', 'Hack Reactor'),
    ('program_2', 'Program 2'),
]


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    fullname = models.CharField(max_length=100, blank=True, default="Unknown")
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    program = models.CharField(max_length=100, choices=PROGRAM_CHOICES, blank=True)
    email = models.EmailField(blank=True)
    links = models.URLField(blank=True)
    social_profiles = models.TextField(blank=True) 
    profile_pic = models.URLField(blank=True, null=True, default="Unknown")


    def __str__(self):
        return self.fullname
