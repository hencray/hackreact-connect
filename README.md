# HackReact Connect

## Description
HackReact Connect is a dynamic platform created for Hack Reactor alumni. 
Showcase your latest projects
Get Inspiration
Find Connections

## Installation
### Requirements
- Python
- Django

### Steps
1. Clone the repository: `git clone [repository link]`
2. Set up a virtual environment: `python -m venv venv`
3. Activate the virtual environment:
   - Windows: `venv\Scripts\activate`
   - MacOS/Linux: `source venv/bin/activate`
4. Install dependencies: `pip install -r requirements.txt`
5. Run migrations: `python manage.py migrate`
6. Start the server: `python manage.py runserver`

## Support
For support, please open an issue in the GitLab repository or contact henrymartija@gmail.com.

## Roadmap

### Completed
- [x] Home Page
- [x] Project List View
- [x] Core Models Setup
- [x] Project Contributors (Many-to-Many Model)

### Short Term
- [ ] Edit profile
- [ ] Registration for new profile
- [ ] Adding a new project
- [ ] Edit project
- [ ] Student directory
- [ ] Project view with filters

### Long Term
- [ ] Photo upload feature
- [ ] Hosting and domain name setup

### Long Long Term
- [ ] Expand to include all bootcamps
- [ ] Implement an invitation system

## Contributing
Contributions are welcome! Please read our contributing guidelines for instructions on how to make a contribution.

## License
This project is licensed under the [MIT License](LICENSE).

## Project Status
This project is actively maintained and open to contributions.
