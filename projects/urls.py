from django.urls import path, include
from projects.views import project_list, project_detail, home, create_project

urlpatterns = [
    path("projects/create_project", create_project, name="create"),
    path("", home, name="home"),
    path("projects/", project_list, name="project_list"),
    path("projects/<int:pk>/", project_detail, name="detail"),
]
