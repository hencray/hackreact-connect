from django.db import models
from profiles.models import Profile

# Create your models here.

class Technology(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Project(models.Model):
    MODULE_CHOICES = [
        ('Module 1', 'Module 1'),
        ('Module 2', 'Module 2'),
        ('Module 3', 'Module 3'),
        ('Other', 'Other'),
    ]

    title = models.CharField(max_length=100)
    description = models.TextField()
    project_link = models.URLField(blank=True)
    image_url = models.URLField(blank=True, null=True)
    contributors = models.ManyToManyField(Profile, related_name='projects')
    module = models.CharField(max_length=20, choices=MODULE_CHOICES)
    technologies = models.ManyToManyField(Technology, related_name='projects', blank=True, default="Unknown")

    
