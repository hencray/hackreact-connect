from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.

def project_list(request):
    projects = Project.objects.all()
    context = {
		'projects': projects,
	}
    return render(request, 'projects/project_list.html', context)

def project_detail(request, pk):
    project = Project.objects.get(pk=pk)
    context = {
		'project': project
	}
    return render(request, 'projects/project_detail.html', context)

def home(request):
    project = Project.objects.all()
    context = {
        'projects': project
    }
    return render(request, 'projects/home.html', context)

def road_map(request):
    return render(request, 'projects/road_map.html')

def create_project(request):
  if request.method == "POST":
    form = ProjectForm(request.POST)
    if form.is_valid():
      form.save()
      # If you need to do something to the model before saving,
      # you can get the instance by calling
      # model_instance = form.save(commit=False)
      # Modifying the model_instance
      # and then calling model_instance.save()
      return redirect("home")
  else:
    form = ProjectForm()

  context = {
    "form": form
  }

  return render(request, "projects/create_project.html", context)
