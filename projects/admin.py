from django.contrib import admin
from projects.models import Project


from .models import Project

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'project_link', 'image_url', 'contributors_list', 'technologies_list']
    filter_horizontal = ('technologies', 'contributors',)

    def contributors_list(self, obj):
        return ", ".join([contributor.fullname for contributor in obj.contributors.all()])
    contributors_list.short_description = 'Contributors'

    def technologies_list(self, obj):
        return ", ".join([tech.name for tech in obj.technologies.all()])
    technologies_list.short_description = 'Technologies'
